<?php

namespace Drupal\webform_edit_element\Element;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Render\Element\RenderElement;
use Drupal\webform\Entity\Webform as WebformEntity;
use Drupal\webform\Entity\WebformSubmission;
use Drupal\webform\WebformInterface;

/**
 * Provides a render element to display a webform.
 *
 * @RenderElement("webform_edit")
 */
class WebformEdit extends RenderElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);
    return [
      '#pre_render' => [
        [$class, 'preRenderWebformElement'],
      ],
      '#sid' => NULL,

    ];
  }

  /**
   * Webform element pre render callback.
   */
  public static function preRenderWebformElement($element) {

    $ws = WebformSubmission::load($element['#sid']);
          // Build the webform.
    $element['webform_edit_build'] = \Drupal::service('entity.form_builder')
      ->getForm($ws, 'edit');


    //unset($element['webform_edit_build']['navigation']);
   // unset($element['webform_edit_build']['information']);




      return $element;
  }



}
